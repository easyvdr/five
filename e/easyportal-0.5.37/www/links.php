<?php
include('includes/kopf.php'); 

echo "<div id='kopf2'>? - Links</div>";

echo "<div id='inhalt'>";
echo "<center>";

include('includes/subnavi_hilfe.php'); 

echo "<h2>Links</h2>";

echo "<div class='links'>";

// *************************************** Container Begin ****************************************************
container_begin(2, "book_link.png", "Forum - VDR-Portal");
echo "<center><a href='https://www.vdr-portal.de/forum/index.php?board/91-easyvdr/' target='_new'><img src='images/links/hp_forum_s_new.jpg' class='responsive_img' style='max-width:400px;'></a></center>";
container_end();
// *************************************** Container Ende ****************************************************

// *************************************** Container Begin ****************************************************
container_begin(2, "book_link.png", "Backup Wiki - ftp.gwdg.de");
echo "<center><a href='https://ftp.gwdg.de/pub/linux/easyvdr/easyvdr-wiki/Hauptseite.htm' target='_new'><img src='images/links/hp_wiki_s.jpg' class='responsive_img' style='max-width:400px;'></a></center>";
container_end();
// *************************************** Container Ende ****************************************************

// *************************************** Container Begin ****************************************************
container_begin(2, "book_link.png", "Backup Forum - ftp.gwdg.de");
echo "<center><a href='https://ftp.gwdg.de/pub/linux/easyvdr/easyvde-forum/www.easy-vdr.de/index.php.htm' target='_new'><img src='images/links/hp_forum_s.jpg' class='responsive_img' style='max-width:400px;'></a></center>";
container_end();
// *************************************** Container Ende ****************************************************

echo "</div>";  // class='links'
echo "<div class='rechts'>";

// *************************************** Container Begin ****************************************************
container_begin(2, "book_link.png", "Ubuntu Pakete");
echo "<center><a href='https://launchpad.net/~easyvdr-team' target='_new'><img src='images/links/hp_ppa_s.jpg' class='responsive_img' style='max-width:400px;'></a></center>";
container_end();
// *************************************** Container Ende ****************************************************

// *************************************** Container Begin ****************************************************
container_begin(2, "book_link.png", "Sourcecode - GitLab");
echo "<center><a href='https://gitlab.com/easyvdr/five' target='_new'><img src='images/links/hp_git_s_new.jpg' class='responsive_img' style='max-width:400px;'></a></center>";
container_end();
// *************************************** Container Ende ****************************************************

echo "</div>";  // class='rechts'
echo "<div class='clear'></div>"; // Ende 2-spaltig

echo "</center>";
echo "</div>";  // Inhalt

include('includes/fuss.php'); 

?>
