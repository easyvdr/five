# VDR language source file.
# Copyright (C) 2008 Klaus Schmidinger <kls@tvdr.de>
# This file is distributed under the same license as the VDR package.
# Dimitrios Dimitrakos <mail@dimitrios.de>, 2002, 2006
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.6.0\n"
"Report-Msgid-Bugs-To: <>\n"
"POT-Creation-Date: 2011-11-20 16:51+0100\n"
"PO-Revision-Date: 2011-11-18 05:30+0100\n"
"Last-Translator: Tomas Saxer <tsaxer@gmx.de>\n"
"Language-Team: Greek\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Channels"
msgstr "Κανάλια"

msgid "Timers"
msgstr "Χρονοπρογραμματισμοί"

#, fuzzy
msgid "Schedule"
msgstr "Πρόγραμμα"

msgid "Setup"
msgstr "Ρυθμισεις"

msgid "Commands"
msgstr "Εντολές"

msgid "Recordings"
msgstr "Εγγραφές"

msgid "Now"
msgstr ""

msgid "Next"
msgstr ""

msgid "Directory browser"
msgstr ""

msgid "hour"
msgstr ""

msgid "cutted"
msgstr ""

msgid "Channel"
msgstr "Κανάλι"

#, fuzzy
msgid "Filesize"
msgstr "Αρχείο"

#, fuzzy
msgid "Volume"
msgstr "Ένταση "

#, fuzzy
msgid "Mute"
msgstr "Σιωπηλό"

#~ msgid "*** Invalid Channel ***"
#~ msgstr "*** Ακυρο κανάλη ***"

#, fuzzy
#~ msgid "Channel locked by LNB!"
#~ msgstr "Το κανάλι είναι αποσχολιμένο (Γίνετε εγγραφή)!"

#~ msgid "Channel not available!"
#~ msgstr "Το κανάλη δέν είναι διαθέσιμο!"

#~ msgid "Can't start Transfer Mode!"
#~ msgstr "Αδυναμία εκκίνησης κατάστασης μεταφοράς!"

#~ msgid "off"
#~ msgstr "κλειστό"

#~ msgid "auto"
#~ msgstr "αυτόματο"

#~ msgid "none"
#~ msgstr "κανένα"

#~ msgid "Polarization"
#~ msgstr "Πόλωση"

#~ msgid "Srate"
#~ msgstr "Srate"

#~ msgid "Inversion"
#~ msgstr "Αντιστροφή"

#~ msgid "CoderateH"
#~ msgstr "Ρυθμός Κώδικα H"

#~ msgid "CoderateL"
#~ msgstr "Ρυθμός Κώδικα L"

#~ msgid "Modulation"
#~ msgstr "Διαμόρφωση"

#~ msgid "Bandwidth"
#~ msgstr "Εύρος Συχνοτήτων"

#~ msgid "Transmission"
#~ msgstr "Μετάδοση"

#~ msgid "Guard"
#~ msgstr "Προστασία"

#~ msgid "Hierarchy"
#~ msgstr "Ιεραρχεία"

#~ msgid "Starting EPG scan"
#~ msgstr "Αρχή σάρωση EPG"

#~ msgid "No title"
#~ msgstr "Χωρίς Τίτλο"

#~ msgid "LanguageName$English"
#~ msgstr "Ελληνικά"

#~ msgid "LanguageCode$eng"
#~ msgstr "ell"

#~ msgid "Phase 1: Detecting RC code type"
#~ msgstr "Φάση 1: Ανίχνευση κώδικα RC"

#~ msgid "Press any key on the RC unit"
#~ msgstr "Πίεσε ένα πλήκτρο στο τηλεχειριστήριο"

#~ msgid "RC code detected!"
#~ msgstr "Βρέθηκε κώδικας RC!"

#~ msgid "Do not press any key..."
#~ msgstr "Μήν πατάς πλήκτρα..."

#~ msgid "Phase 2: Learning specific key codes"
#~ msgstr "Φαση 2: Εκμάθηση μεμονομένων πλήκτρων"

#~ msgid "Press key for '%s'"
#~ msgstr "Πίεσε το πλήκτρο γιά '%s'"

#~ msgid "Press 'Up' to confirm"
#~ msgstr "Πίεσε 'πάνω' γιά αποδοχή"

#~ msgid "Press 'Down' to continue"
#~ msgstr "Πίεσε 'κάτω' γιά συνέχεια"

#~ msgid "(press 'Up' to go back)"
#~ msgstr "(Πίεσε 'πάνω' γιά επιστροφή"

#~ msgid "(press 'Down' to end key definition)"
#~ msgstr "(Πίεσε 'κάτω' γιά τερματισμό δηλώσεων πλήκτρων)"

#~ msgid "(press 'Menu' to skip this key)"
#~ msgstr "Πίεσε 'μενού' για προσπέραση αυτού τού πλήκτρου"

#~ msgid "Learning Remote Control Keys"
#~ msgstr "Εκμάθηση πλήκτρων τηλεχειρισμού"

#~ msgid "Phase 3: Saving key codes"
#~ msgstr "Φαση 3: Αποθήκευση κωδικών"

#~ msgid "Press 'Up' to save, 'Down' to cancel"
#~ msgstr "Πίεσε 'Πάνω' για αποθήκευση, 'Κάτω' για ακύρωση"

#~ msgid "Key$Up"
#~ msgstr "Πάνω"

#~ msgid "Key$Down"
#~ msgstr "Κάτω"

#~ msgid "Key$Menu"
#~ msgstr "Μενού"

#~ msgid "Key$Ok"
#~ msgstr "Οκ"

#~ msgid "Key$Back"
#~ msgstr "Πίσω"

#~ msgid "Key$Left"
#~ msgstr "Αριστερά"

#~ msgid "Key$Right"
#~ msgstr "Δεξιά"

#~ msgid "Key$Red"
#~ msgstr "Κόκκινο"

#~ msgid "Key$Green"
#~ msgstr "Πράσινο"

#~ msgid "Key$Yellow"
#~ msgstr "Κίτρινο"

#~ msgid "Key$Blue"
#~ msgstr "Μπλέ"

#~ msgid "Key$Info"
#~ msgstr "Πληροφορίες"

#~ msgid "Key$Play"
#~ msgstr "Αναπαραγωγή"

#~ msgid "Key$Pause"
#~ msgstr "Παύση"

#~ msgid "Key$Stop"
#~ msgstr "Tέρμα"

#~ msgid "Key$Record"
#~ msgstr "Eγγραφή"

#~ msgid "Key$FastFwd"
#~ msgstr "Προώθηση εμπρός"

#~ msgid "Key$FastRew"
#~ msgstr "Προώθηση πίσω"

#~ msgid "Key$Power"
#~ msgstr "Kλείσιμο"

#~ msgid "Key$Channel+"
#~ msgstr "Κανάλι+"

#~ msgid "Key$Channel-"
#~ msgstr "Κανάλι-"

#~ msgid "Key$Volume+"
#~ msgstr "Ένταση+"

#~ msgid "Key$Volume-"
#~ msgstr "Ένταση-"

#~ msgid "Key$Audio"
#~ msgstr "Ήχος"

#~ msgid "Key$Schedule"
#~ msgstr "Πρόγραμμα"

#~ msgid "Key$Channels"
#~ msgstr "Κανάλια"

#~ msgid "Key$Timers"
#~ msgstr "Χρονοπρογραμματισμοί"

#~ msgid "Key$Recordings"
#~ msgstr "Εγγραφές"

#~ msgid "Key$Setup"
#~ msgstr "Ρυθμισεις"

#~ msgid "Key$Commands"
#~ msgstr "Εντολές"

#~ msgid "Disk"
#~ msgstr "Δίσκος"

#~ msgid "free"
#~ msgstr "Ελεύθερος"

#~ msgid "Edit channel"
#~ msgstr "Τροποποίηση Καναλιού"

#~ msgid "Name"
#~ msgstr "Όνομα"

#~ msgid "Source"
#~ msgstr "Πηγή"

#~ msgid "Frequency"
#~ msgstr "Συχνότητα"

#~ msgid "Vpid"
#~ msgstr "Bίντεο PID"

#~ msgid "Ppid"
#~ msgstr "Ppid"

#~ msgid "Apid1"
#~ msgstr "Apid1"

#~ msgid "Apid2"
#~ msgstr "Apid2"

#~ msgid "Dpid1"
#~ msgstr "Dpid1"

#~ msgid "Dpid2"
#~ msgstr "Dpid2"

#~ msgid "Tpid"
#~ msgstr "Tpid"

#~ msgid "CA"
#~ msgstr "CA"

#~ msgid "Sid"
#~ msgstr "Sid"

#, fuzzy
#~ msgid "Rid"
#~ msgstr "Sid"

#~ msgid "Channel settings are not unique!"
#~ msgstr "Οι ριθμύσεις τον καναλιών αλλυλοσυμπίπτουν!"

#~ msgid "Button$Edit"
#~ msgstr "Προσαρμογή"

#~ msgid "Button$New"
#~ msgstr "Nέο"

#~ msgid "Button$Delete"
#~ msgstr "Διαγραφή"

#~ msgid "Button$Mark"
#~ msgstr "Επιλογή"

#~ msgid "Channel is being used by a timer!"
#~ msgstr "Το κανάλη χρισιμοποιείται από χρονοπρογραμματισμό!"

#~ msgid "Delete channel?"
#~ msgstr "Διαγραφή καναλιού?"

#~ msgid "Edit timer"
#~ msgstr "Τροποποίηση χρονοπρογραμματισμού"

#~ msgid "Active"
#~ msgstr "Ενεργό"

#~ msgid "Day"
#~ msgstr "Ημέρα"

#~ msgid "Start"
#~ msgstr "Αρχή"

#~ msgid "Stop"
#~ msgstr "Τέλος"

#~ msgid "VPS"
#~ msgstr "VPS"

#~ msgid "Priority"
#~ msgstr "Προτεραιότητα"

#~ msgid "Lifetime"
#~ msgstr "Διέρκεια Παραμονής"

#~ msgid "yes"
#~ msgstr "ναί"

#~ msgid "no"
#~ msgstr "όχι"

#~ msgid "First day"
#~ msgstr "Πρώτη μέρα"

#~ msgid "Button$On/Off"
#~ msgstr "Aνοιχ/Kλειστό"

#~ msgid "Button$Info"
#~ msgstr "Πληροφορίες"

#~ msgid "Delete timer?"
#~ msgstr "Διαγραφή χρονοπρογραμματισμού;?"

#~ msgid "Timer still recording - really delete?"
#~ msgstr "Χρονοπρογραμματισμός σέ εξέλιξη - Διαγραφή σίγουρα?"

#~ msgid "Event"
#~ msgstr "Εκπομπή"

#~ msgid "Button$Record"
#~ msgstr "Εγγραφή"

#~ msgid "Button$Switch"
#~ msgstr "Aλλαγή"

#~ msgid "What's on now?"
#~ msgstr "Τρέχον πρόγραμμα"

#~ msgid "What's on next?"
#~ msgstr "Επόμενο πρόγραμμα"

#~ msgid "Button$Next"
#~ msgstr "Επόμενο"

#~ msgid "Button$Now"
#~ msgstr "Τώρα"

#~ msgid "Button$Schedule"
#~ msgstr "Πρόγραμμα"

#~ msgid "Can't switch channel!"
#~ msgstr "Αλλαγή καναλιού αδύνατη!"

#~ msgid "Schedule - %s"
#~ msgstr "Πρόγραμμα - %s"

#~ msgid "Recording info"
#~ msgstr "Πληροφορίες Eγγραφής"

#~ msgid "Button$Play"
#~ msgstr "Αναπαραγωγή"

#~ msgid "Button$Rewind"
#~ msgstr "Μεταφορά στην Αρχή"

#, fuzzy
#~ msgid "Rename recording"
#~ msgstr "Διαγραφή εγγραφής?"

#, fuzzy
#~ msgid "Date"
#~ msgstr "Srate"

#, fuzzy
#~ msgid "PES"
#~ msgstr "VPS"

#, fuzzy
#~ msgid "Delete marks information?"
#~ msgstr "Διαγραφή εγγραφής?"

#, fuzzy
#~ msgid "Delete resume information?"
#~ msgstr "Διαγραφή εγγραφής?"

#~ msgid "Error while accessing recording!"
#~ msgstr "Πρόβλημα στήν προσπέλαση εγγραφής!"

#~ msgid "Button$Open"
#~ msgstr "Ανοιγμα"

#~ msgid "Delete recording?"
#~ msgstr "Διαγραφή εγγραφής?"

#~ msgid "Error while deleting recording!"
#~ msgstr "Λάθος κατά τήν διαγραφή του αρχείου!"

#~ msgid "Recording commands"
#~ msgstr "Εντολές γιά εγγραφές"

#~ msgid "never"
#~ msgstr "Ποτέ"

#~ msgid "skin dependent"
#~ msgstr "Εξαρτάτε από τήν επιφάνεια"

#~ msgid "always"
#~ msgstr "πάντα"

#~ msgid "OSD"
#~ msgstr "OSD"

#~ msgid "Setup.OSD$Language"
#~ msgstr "Γλώσσα"

#~ msgid "Setup.OSD$Skin"
#~ msgstr "Επιφάνεια"

#~ msgid "Setup.OSD$Theme"
#~ msgstr "Θέμα"

#, fuzzy
#~ msgid "Setup.OSD$WarEagle icons"
#~ msgstr "Γλώσσα"

#~ msgid "Setup.OSD$Left (%)"
#~ msgstr "Αριστερά (%)"

#~ msgid "Setup.OSD$Top (%)"
#~ msgstr "Επάνω (%)"

#~ msgid "Setup.OSD$Width (%)"
#~ msgstr "Μάκρος (%)"

#~ msgid "Setup.OSD$Height (%)"
#~ msgstr "Ύψος (%)"

#~ msgid "Setup.OSD$Message time (s)"
#~ msgstr "Χρόνος ένδειξης μυνημάτων (δ)"

#~ msgid "Setup.OSD$Use small font"
#~ msgstr "Χρισημοποίηση μικρόν γραμματοσειρών"

#~ msgid "Setup.OSD$Channel info position"
#~ msgstr "Θέση πληροφορίας καναλιών"

#~ msgid "bottom"
#~ msgstr "κάτω"

#~ msgid "top"
#~ msgstr "πάνω"

#~ msgid "Setup.OSD$Channel info time (s)"
#~ msgstr "χρόνος ένδεικσης πληροφορίων καναλιού σε (δ)"

#~ msgid "Setup.OSD$Info on channel switch"
#~ msgstr "Πληροφορίες στήν αλλαγή καναλιού"

#~ msgid "Setup.OSD$Scroll pages"
#~ msgstr "Κύλιση σελίδας"

#~ msgid "Setup.OSD$Scroll wraps"
#~ msgstr "Κύλιση γύρω-γύρω"

#~ msgid "Setup.OSD$Recording directories"
#~ msgstr "Φάκελοι εγγραφών"

#, fuzzy
#~ msgid "Setup.OSD$Main menu command position"
#~ msgstr "Θέση πληροφορίας καναλιών"

#, fuzzy
#~ msgid "Setup.OSD$Show valid input"
#~ msgstr "Επιφάνεια"

#~ msgid "EPG"
#~ msgstr "Ηλεκτρονικός οδηγός προγράμματος"

#~ msgid "Button$Scan"
#~ msgstr "Σάρωση"

#~ msgid "Setup.EPG$EPG scan timeout (h)"
#~ msgstr "Χρόνος διάρκειας εξέτασης EPG σε ώρες"

#~ msgid "Setup.EPG$EPG bugfix level"
#~ msgstr "Βαθμός διόρθωσης οδηγού EPG"

#~ msgid "Setup.EPG$EPG linger time (min)"
#~ msgstr "Ένδειξη ξεπερασμένων πληροφοριών (λεπτά)"

#~ msgid "Setup.EPG$Set system time"
#~ msgstr "Συντονισμός ώρας υπολογιστή"

#~ msgid "Setup.EPG$Use time from transponder"
#~ msgstr "Αναμεταδότης συντονισμού ώρας"

#~ msgid "Setup.EPG$Preferred languages"
#~ msgstr "Προτεινόμενες γλώσσες"

#~ msgid "Setup.EPG$Preferred language"
#~ msgstr "Προτεινόμενη γλώσσα"

#~ msgid "pan&scan"
#~ msgstr "pan&scan"

#~ msgid "letterbox"
#~ msgstr "letterbox"

#~ msgid "center cut out"
#~ msgstr "center cut out"

#~ msgid "names only"
#~ msgstr "μόνο ονόματα"

#~ msgid "names and PIDs"
#~ msgstr "Ονόματα καί PIDs"

#~ msgid "add new channels"
#~ msgstr "προσθήκη νέων καναλιών"

#~ msgid "add new transponders"
#~ msgstr "προσθήκη νέου αναμεταδότη"

#~ msgid "DVB"
#~ msgstr "DVB"

#, fuzzy
#~ msgid "Setup.DVB$Use DVB receivers"
#~ msgstr "Χρήση ήχου Dolby Digital"

#~ msgid "Setup.DVB$Primary DVB interface"
#~ msgstr "Κύρια DVB κάρτα"

#~ msgid "Setup.DVB$Video format"
#~ msgstr "Μορφή οθόνης"

#~ msgid "Setup.DVB$Video display format"
#~ msgstr "Μορφή απεικόνισης Βίντεο"

#~ msgid "Setup.DVB$Use Dolby Digital"
#~ msgstr "Χρήση ήχου Dolby Digital"

#~ msgid "Setup.DVB$Update channels"
#~ msgstr "Ενημέρωση καναλιών"

#, fuzzy
#~ msgid "Setup.DVB$channel binding by Rid"
#~ msgstr "Θέση πληροφορίας καναλιών"

#~ msgid "Setup.DVB$Audio languages"
#~ msgstr "Γλώσσες ήχου"

#~ msgid "Setup.DVB$Audio language"
#~ msgstr "Γλώσσα ήχου"

#~ msgid "LNB"
#~ msgstr "LNB"

#, fuzzy
#~ msgid "Setup.LNB$Log LNB usage"
#~ msgstr "Κάτω LNB-Συχνότητα (MHz)"

#~ msgid "Setup.LNB$Use DiSEqC"
#~ msgstr "Ενεργοποίηση DiSEqC"

#~ msgid "Setup.LNB$SLOF (MHz)"
#~ msgstr "SLOF (MHz)"

#~ msgid "Setup.LNB$Low LNB frequency (MHz)"
#~ msgstr "Κάτω LNB-Συχνότητα (MHz)"

#~ msgid "Setup.LNB$High LNB frequency (MHz)"
#~ msgstr "’νω LNB-Συχνότητα (MHz)"

#~ msgid "CAM"
#~ msgstr "CAM"

#~ msgid "Button$Menu"
#~ msgstr "Mενού"

#~ msgid "Button$Reset"
#~ msgstr "Επαναφορά"

#~ msgid "Can't open CAM menu!"
#~ msgstr "Αδύνατη η πρόσβαση στό CAM μενού!"

#~ msgid "Can't reset CAM!"
#~ msgstr "Αδύνατη η επαναφορά στό CAM"

#~ msgid "Recording"
#~ msgstr "Εγγραφή"

#~ msgid "Setup.Recording$Margin at start (min)"
#~ msgstr "Πρόσθετος χρόνος στην αρχή (λεπτά)"

#~ msgid "Setup.Recording$Margin at stop (min)"
#~ msgstr "Πρόσθετος χρόνος στό τέλος (λεπτά)"

#~ msgid "Setup.Recording$Primary limit"
#~ msgstr "Προτεύον όριο"

#~ msgid "Setup.Recording$Default priority"
#~ msgstr "Προκαθορισμένη προτεραιότητα"

#~ msgid "Setup.Recording$Default lifetime (d)"
#~ msgstr "Προκαθορισμένη διάρκεια παραμονής (Ημέρες)"

#~ msgid "Setup.Recording$Pause priority"
#~ msgstr "Προτεραιότητα διαλείμματος"

#~ msgid "Setup.Recording$Pause lifetime (d)"
#~ msgstr "Διάρκεια διαλείματος"

#, fuzzy
#~ msgid "Setup.Recording$Video directory policy"
#~ msgstr "Φάκελοι εγγραφών"

#, fuzzy
#~ msgid "Setup.Recording$Number of video directories"
#~ msgstr "Φάκελοι εγγραφών"

#, fuzzy
#~ msgid "Setup.Recording$Video %d priority"
#~ msgstr "Προτεραιότητα διαλείμματος"

#, fuzzy
#~ msgid "Setup.Recording$Video %d min. free MB"
#~ msgstr "Μέγιστο μέγεθος αρχείου (MB)"

#~ msgid "Setup.Recording$Use episode name"
#~ msgstr "Χρήση ονόματος επεισοδίου"

#~ msgid "Setup.Recording$Use VPS"
#~ msgstr "Χρήση VPS"

#~ msgid "Setup.Recording$VPS margin (s)"
#~ msgstr "Περιθώριο VPS (δ)"

#~ msgid "Setup.Recording$Mark instant recording"
#~ msgstr "Επιλογή τρέχουσας εγγραφής"

#~ msgid "Setup.Recording$Name instant recording"
#~ msgstr "Ονομασία τρέχουσας εγγραφής"

#~ msgid "Setup.Recording$Instant rec. time (min)"
#~ msgstr "Διάρκεια στγμιαίας εγγραφής (λεπτά)"

#~ msgid "Setup.Recording$Max. video file size (MB)"
#~ msgstr "Μέγιστο μέγεθος αρχείου (MB)"

#, fuzzy
#~ msgid "Setup.Recording$Max. recording size (GB)"
#~ msgstr "Μέγιστο μέγεθος αρχείου (MB)"

#, fuzzy
#~ msgid "Setup.Recording$Hard Link Cutter"
#~ msgstr "Επιλογή τρέχουσας εγγραφής"

#~ msgid "Setup.Recording$Split edited files"
#~ msgstr "Διαμέλισμός επεξεργασμένων αρχείων"

#, fuzzy
#~ msgid "Setup.Recording$Show date"
#~ msgstr "Διαμέλισμός επεξεργασμένων αρχείων"

#, fuzzy
#~ msgid "Setup.Recording$Show time"
#~ msgstr "Διάρκεια διαλείματος"

#, fuzzy
#~ msgid "Setup.Recording$Show length"
#~ msgstr "Χρήση VPS"

#~ msgid "Replay"
#~ msgstr "Αναπαραγωγή"

#~ msgid "Setup.Replay$Multi speed mode"
#~ msgstr "Μεθοδος πολλαπλής ταχύτητας"

#~ msgid "Setup.Replay$Show replay mode"
#~ msgstr "Ένδειξη κατάστασης αναμετάδοσης"

#~ msgid "Setup.Replay$Resume ID"
#~ msgstr "ID αναμετάδοσης"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds"
#~ msgstr "Χρήση VPS"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds Slow"
#~ msgstr "Χρήση VPS"

#, fuzzy
#~ msgid "Setup.Recording$Jump Seconds (Repeat)"
#~ msgstr "Χρήση ονόματος επεισοδίου"

#, fuzzy
#~ msgid "Setup.Replay$Jump&Play"
#~ msgstr "ID αναμετάδοσης"

#, fuzzy
#~ msgid "Setup.Replay$Play&Jump"
#~ msgstr "ID αναμετάδοσης"

#, fuzzy
#~ msgid "Setup.Replay$Pause at last mark"
#~ msgstr "Μεθοδος πολλαπλής ταχύτητας"

#, fuzzy
#~ msgid "Setup.Replay$Reload marks"
#~ msgstr "ID αναμετάδοσης"

#, fuzzy
#~ msgid "Setup.Miscellaneous$only in channelinfo"
#~ msgstr "Ελάχιστος χράνος παρεμβολής (λεπτά)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$only in progress display"
#~ msgstr "Ζάπινγκ διακοπή (δ)"

#~ msgid "Miscellaneous"
#~ msgstr "Διάφορα"

#~ msgid "Setup.Miscellaneous$Min. event timeout (min)"
#~ msgstr "Ελάχιστος χράνος παρεμβολής (λεπτά)"

#~ msgid "Setup.Miscellaneous$Min. user inactivity (min)"
#~ msgstr "Ελάχιστος χρόνος αναμονής (λεπτά)"

#~ msgid "Setup.Miscellaneous$SVDRP timeout (s)"
#~ msgstr "SVDRP διακοπή (δ)"

#~ msgid "Setup.Miscellaneous$Zap timeout (s)"
#~ msgstr "Ζάπινγκ διακοπή (δ)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat delay"
#~ msgstr "Ελάχιστος χράνος παρεμβολής (λεπτά)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat freq"
#~ msgstr "Ζάπινγκ διακοπή (δ)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Lirc repeat timeout"
#~ msgstr "Ελάχιστος χράνος παρεμβολής (λεπτά)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Volume ctrl with left/right"
#~ msgstr "Ελάχιστος χρόνος αναμονής (λεπτά)"

#, fuzzy
#~ msgid "Setup.Miscellaneous$Channelgroups with left/right"
#~ msgstr "Ζάπινγκ διακοπή (δ)"

#~ msgid "Plugins"
#~ msgstr "Επεκτάσεις"

#~ msgid "This plugin has no setup parameters!"
#~ msgstr "Αυτή η επέκταση δεν έχει παράμετρους!"

#~ msgid "Restart"
#~ msgstr "Επανεκκίνηση"

#~ msgid "Really restart?"
#~ msgstr "Nα γίνει σίγουρα επανεκκίνηση?"

#~ msgid " Stop recording "
#~ msgstr " Τέλος εγγαφής "

#~ msgid "VDR"
#~ msgstr "VDR"

#~ msgid " Stop replaying"
#~ msgstr " Τέλος αναπαραγωγής"

#~ msgid "Button$Audio"
#~ msgstr "Ήχος"

#~ msgid "Button$Pause"
#~ msgstr "Παύση"

#~ msgid "Button$Stop"
#~ msgstr "Τέρμα"

#~ msgid "Button$Resume"
#~ msgstr "Επαναφορά"

#~ msgid " Cancel editing"
#~ msgstr " Ακύρωση επεξεργασίας"

#~ msgid "Stop recording?"
#~ msgstr "Ακύρωση εγγραφής?"

#~ msgid "Cancel editing?"
#~ msgstr "Aκύρωση επεξεργασίας?"

#~ msgid "No audio available!"
#~ msgstr "Μη διαθέσιμος ήχος"

#~ msgid "No free DVB device to record!"
#~ msgstr "Ανεπάρκεια DVB Κάρτας γιά εγγραφή!"

#~ msgid "Pausing live video..."
#~ msgstr "Πάγωμα ζωντανού σήματος"

#~ msgid "Jump: "
#~ msgstr "Τοποθέτηση: "

#~ msgid "No editing marks defined!"
#~ msgstr "Δέν έχουν οριστεί σημεία επεξεργασίας"

#~ msgid "Can't start editing process!"
#~ msgstr "Αδυναμία εκκίνησης της επεξεργασίας!"

#~ msgid "Editing process started"
#~ msgstr "Αρχισε η επεξεργασία"

#~ msgid "Editing process already active!"
#~ msgstr "Επεξεργασία βρίσκεται σέ εξέλιξη!"

#~ msgid "FileNameChars$ abcdefghijklmnopqrstuvwxyz0123456789-.,#~\\^$[]|()*+?{}/:%@&"
#~ msgstr " αάβγδεέζηήθιίκλμνξοόπρσςτυύφχψωώ0123456789-.,#~\\^$[]|()*+?{}/:%@&abcdefghijklmnopqrstuvwxyz"

#~ msgid "Button$ABC/abc"
#~ msgstr "ABΓ/αβγ"

#~ msgid "Button$Overwrite"
#~ msgstr "Αντικατάσταση"

#~ msgid "Button$Insert"
#~ msgstr "Εισαγωγή"

#~ msgid "Plugin"
#~ msgstr "Επέκταση"

#~ msgid "Up/Dn for new location - OK to move"
#~ msgstr "Πάνω/Κάτω γιά νέα θέση. Μετά ΟΚ"

#~ msgid "Channel locked (recording)!"
#~ msgstr "Το κανάλι είναι αποσχολιμένο (Γίνετε εγγραφή)!"

#~ msgid "Low disk space!"
#~ msgstr "Ο σκληρός κοντεύει νά γεμίσει!"

#~ msgid "Can't shutdown - option '-s' not given!"
#~ msgstr "Αδύνατον να γίνει τερματισμός. Ανύπαρκτη η παράμετρος '-s'!"

#~ msgid "Recording - shut down anyway?"
#~ msgstr "Γίνεται εγγραφή - Τελικά να γίνει τερματισμός?"

#~ msgid "Recording in %ld minutes, shut down anyway?"
#~ msgstr "Αναμένεται εγγραφή σέ %ld λεπτά - Τελικά να τερματιστεί?"

#~ msgid "shut down anyway?"
#~ msgstr "Τελικά να γίνει τερματισμός?"

#~ msgid "Recording - restart anyway?"
#~ msgstr "Γίνεται εγγραφή - Τελικά να γίνει επανεκκίνηση?"

#~ msgid "restart anyway?"
#~ msgstr "Τελικά να γίνει επανεκκίνηση?"

#~ msgid "Classic VDR"
#~ msgstr "Κλασικό VDR"

#~ msgid "ST:TNG Panels"
#~ msgstr "Μορφές ST:TNG"

#~ msgid "MTWTFSS"
#~ msgstr "ΔΤΤΠΠΣΚ"

#~ msgid "MonTueWedThuFriSatSun"
#~ msgstr "ΔευΤρίΤετΠέμΠαρΣάβKυρ"

#~ msgid "Monday"
#~ msgstr "Δευτέρα"

#~ msgid "Tuesday"
#~ msgstr "Τρίτη"

#~ msgid "Wednesday"
#~ msgstr "Τετάρτη"

#~ msgid "Thursday"
#~ msgstr "Πέμπτη"

#~ msgid "Friday"
#~ msgstr "Παρασκευή"

#~ msgid "Saturday"
#~ msgstr "Σάββατο"

#~ msgid "Sunday"
#~ msgstr "Κυριακή"

#~ msgid "Press any key to cancel shutdown"
#~ msgstr "Πίεσε ένα πλήκτρο γιά ακύρωδη τερματισμού"

#~ msgid "Switching primary DVB..."
#~ msgstr "Η κύρια DVB κάρτα αλλάζει..."

#~ msgid "Editing process failed!"
#~ msgstr "Η επεξεργασία απέτυχε!"

#~ msgid "Editing process finished"
#~ msgstr "Η επεξεργασία τελείωσε"
