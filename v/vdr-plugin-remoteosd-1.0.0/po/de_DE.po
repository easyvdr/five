# VDR plugin language source file.
# Copyright (C) 2007 Klaus Schmidinger <kls@cadsoft.de>
# This file is distributed under the same license as the VDR package.
#
msgid ""
msgstr ""
"Project-Id-Version: VDR 1.5.7\n"
"Report-Msgid-Bugs-To: <vdrdev@schmirler.de>\n"
"POT-Creation-Date: 2013-05-01 00:08+0200\n"
"PO-Revision-Date: 2009-08-25 00:55+0200\n"
"Last-Translator: Frank Schmirler <vdrdev@schmirler.de>\n"
"Language-Team: <vdr@linuxtv.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-15\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "Remote OSD already in use. Proceed anyway?"
msgstr "Men� bereits in Benutzung. Dennoch fortfahren?"

msgid "Show menu of a remote VDR"
msgstr "Zugriff auf Men� eines anderen VDR"

msgid "Remote menu"
msgstr "Server Men�"

msgid "Remote menu not available. Connection failed."
msgstr "Server Men� nicht verf�gbar. Verbindung fehlgeschlagen."

msgid "Hide mainmenu entry"
msgstr "Hauptmen�eintrag verstecken"

msgid "Replace mainmenu \"Schedule\""
msgstr "Hauptmen� \"Programm\" ersetzen"

msgid "Replace mainmenu \"Timers\""
msgstr "Hauptmen� \"Timer\" ersetzen"

msgid "Server IP"
msgstr "Server IP"

msgid "Server port"
msgstr "Server Port"

msgid "from svdrpservice"
msgstr "aus svdrpservice"

msgid "Tune server channel"
msgstr "Kanal auf Server umstellen"

msgid "Number of lines per page"
msgstr "Zeilen pro Bildschirmseite"

msgid "Remote menu theme"
msgstr "Thema f�r entferntes Men�"
