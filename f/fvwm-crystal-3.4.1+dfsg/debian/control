Source: fvwm-crystal
Section: x11
Priority: optional
Maintainer: Vincent Bernat <bernat@debian.org>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: xsltproc, docbook-xsl
Standards-Version: 3.9.7
Vcs-Browser: https://salsa.debian.org/debian/fvwm-crystal
Vcs-Git: https://salsa.debian.org/debian/fvwm-crystal.git
Homepage: http://fvwm-crystal.sourceforge.net/

Package: fvwm-crystal
Architecture: all
Depends: ${misc:Depends}, python3,
 fvwm (>= 1:2.5.13),
 rxvt-unicode | x-terminal-emulator,
 imagemagick, trayer | stalonetray, hsetroot,
 gawk, xdg-user-dirs
Recommends: xscreensaver,
 audacious | mpd | cdcd | quodlibet, mpc, pmount
Suggests: sudo, menu, rox-filer, nautilus
Provides: x-window-manager
Description: Pretty Desktop Environment based on FVWM
 FVWM-crystal creates an easy to use desktop environment
 using fvwm2 as its window manager and main core. From
 another point of view its just a very powerful fvwm
 configuration.
 .
 A file manager may be optionally used to display desktop icons,
 ROX-filer and nautilus are supported for this task. FVWM-crystal
 furthermore has UI integration for various music players, among
 them audacious and mpd.
 .
 There is also a very powerful menu system that has an extensive
 default configuration but may be customized and extended by each
 user to fit personal requirements.
