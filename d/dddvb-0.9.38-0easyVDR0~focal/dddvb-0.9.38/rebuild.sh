#!/bin/bash
#
## rebuild dddvb modules for new kernel ##
# mango 2019
#
cd /usr/src/dddvb-0.9.38
   dkms build dddvb/0.9.38 -k $(uname -r)
   sleep 0.2
   dkms install dddvb/0.9.38 -k $(uname -r)
